import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_pip_installation(pypi_host, host):
    cmd = host.run(
        f'sudo pip3 install --index-url https://{pypi_host}/simple'
        f' idna --upgrade --force-reinstall'
        )

    assert cmd.rc == 0
