import os
import pytest
import testinfra.utils.ansible_runner


@pytest.fixture()
def pypi_host():
    testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
        os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('pip')

    for hostname in testinfra_hosts:
        host = testinfra.utils.ansible_runner.AnsibleRunner(
            os.environ['MOLECULE_INVENTORY_FILE']
        ).get_host(hostname)

        return host.interface('eth0').addresses[0]
