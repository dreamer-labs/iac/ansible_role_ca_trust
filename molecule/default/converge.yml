---
- name: Converge
  hosts: all
  become: true
  vars:
    cert_name: test_selfsigned
    molecule_run_dir: "{{ lookup('env', 'MOLECULE_EPHEMERAL_DIRECTORY') }}"
  pre_tasks:
    - block:
        - name: Create temporary dir for run
          file:
            path: "{{ molecule_run_dir }}/tmp"
            state: directory
          register: cert_tmpdir

        - name: Create CA key
          community.crypto.openssl_privatekey:
            path: "{{ cert_tmpdir.path }}/CA_key.pem"
          register: ca_key

        - name: Create the CA CSR
          community.crypto.openssl_csr:
            path: "{{ cert_tmpdir.path }}/CA.csr"
            privatekey_path: "{{ ca_key.filename }}"
            basic_constraints:
              - CA:TRUE
            common_name: "my-ca"
          register: ca_csr

        - name: sign the CA CSR
          community.crypto.x509_certificate:
            path: "{{ cert_tmpdir.path }}/CA.crt"
            csr_path: "{{ ca_csr.filename }}"
            privatekey_path: "{{ ca_key.filename }}"
            provider: selfsigned
          register: ca_crt

        - name: Generate an OpenSSL private key with the default values (4096 bits, RSA)
          community.crypto.openssl_privatekey:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.pem"
          register: test_key

        - name: Generate an OpenSSL Certificate Signing Request
          community.crypto.openssl_csr:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.csr"
            privatekey_path: "{{ test_key.filename }}"
            common_name: graylog.example.com
            subject_alt_name:
              - "IP:{{ hostvars[inventory_hostname]['ansible_default_ipv4']['address'] }}"
              - "IP:127.0.0.1"
              - "DNS:localhost"
          register: test_csr

        - name: Generate a Self Signed OpenSSL certificate
          community.crypto.x509_certificate:
            path: "{{ cert_tmpdir.path }}/{{ cert_name }}.crt"
            privatekey_path: "{{ test_key.filename }}"
            csr_path: "{{ test_csr.filename }}"
            ownca_path: "{{ ca_crt.filename }}"
            ownca_privatekey_path: "{{ ca_key.filename }}"
            provider: ownca
          register: test_crt

      run_once: true
      become: false
      delegate_to: localhost

    - name: Copy openSSL files
      copy:
        src: "{{ item.file }}"
        dest: "{{ item.dest }}"
      loop:
        - dest: "/tmp/test_crt.crt"
          file: "{{ test_crt.filename }}"

  tasks:
    - name: "Include ansible_role_ca_trust"
      include_role:
        name: "ansible_role_ca_trust"
      vars:
        ca_trust_certificates:
          - name: custom_certificate.crt
            pem: "{{ lookup('file', ca_crt.filename) }}"
