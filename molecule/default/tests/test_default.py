import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_certificate_valid(host):
    cmd = host.run("openssl verify /tmp/test_crt.crt")

    assert cmd.rc == 0
